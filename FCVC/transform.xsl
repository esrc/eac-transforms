<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:doc="urn:isbn:1-931666-33-4"
    xmlns:relation="urn:isbn:1-931666-22-9"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="html" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
<html>
  <head>
    <title><xsl:call-template name="title" /></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  
    <link rel="stylesheet" href="/static/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/static/css/bootstrap-responsive.min.css" />
    <xsl:comment>Le fav and touch icons</xsl:comment>
    <xsl:comment>Commented out - update these with your own files</xsl:comment>
    <xsl:comment>
      &lt;link rel="shortcut icon" href="ico/favicon.ico" /&gt;
      &lt;link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png" /&gt;
      &lt;link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png" /&gt;
      &lt;link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png" /&gt;
      &lt;link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" /&gt;
    </xsl:comment>

    <!--[if lt IE 9]>
        <script src="/static/js/html5shiv.js"></script>
    <![endif]-->

  </head>
  <body>
    <div class="container-fluid">
      <span id="top"></span>
      <div class="row-fluid">
        <h1><xsl:call-template name="title" /></h1>
        <p><xsl:call-template name="description" /></p>
        <xsl:call-template name='link-to-encyclopaedic-entry' />
      </div>
    </div>
    <script type="text/javascript" src="/static/js/jquery-1.9.1.min.js">//</script>
    <script type="text/javascript" src="/static/js/bootstrap.min.js">//</script>
  </body>
</html>
  </xsl:template>

  <xsl:template name='title'>
    <xsl:value-of select="/doc:eac-cpf/doc:cpfDescription/doc:identity/doc:nameEntry/doc:part" />
  </xsl:template>

  <xsl:template name='description'>
    <xsl:value-of select="//doc:cpfDescription/doc:description/doc:biogHist/doc:abstract" /> 
  </xsl:template>
  
  <xsl:template name='link-to-encyclopaedic-entry'>
    <xsl:variable name='link'>
        <xsl:value-of select="/doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityId" 
            disable-output-escaping="yes" />
    </xsl:variable>
    <a href="{$link}">read more...</a>
  </xsl:template>

</xsl:stylesheet>



